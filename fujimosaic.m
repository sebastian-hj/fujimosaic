%% 
% Some variables
filePath = 'DSCF0399.RAF';
resolution = [6000 4000];

% Read raw file
fid = fopen(filePath,'r');
raw = fread(fid, 'uint16');
fclose(fid);

%%
% % Read raw file
% fid = fopen(filePath,'r');
% header = char(fread(fid, info.header_size_in_bytes)');
% raw = fread(fid,[info.res(1) info.res(2)],'uint16=>uint16',info.endianness);
% fclose(fid);
% raw=(raw(1:info.res(1),:));
% raw = single(raw);
% % Transpose to keep row/column from original capture
% % 'rawCircle' is 32 rows by 64 columns
% rawCircle=raw';

%% Figuring out format...
% Real data seems to start from 0x1027B (66171) until 0x58E4B (364107)
% False alarm...

% Let's see at 388610 (0x5EE02) till 396208 (0x60BB0) (gap till 397220
% (0x60FA4))

n    = 360000;
step =  10000;
while n<length(raw)
    plot(raw(n:n+step));
    n = n + 10000;
    pause
end

% Again... start at 388609 (0x5EE01) till 400672 (0x61D20) (with gap until
% 400928 (0x61E20))

%%
% So... data seems to be packed in 16-bit little-endian per rows...
% In 16-bit format data starts at 194305 (0x2F701) [or could be 194304
% (0x2F700)] until 200336 (0x30E90) (with gap until 200464 (0x30F10))

n    = 194305;
step =  200464-194305;
i = 1;
while n<length(raw)
    plot(raw(n:n+step));
    title({['Location ' num2str(n)], ...
           ['Left ' num2str(length(raw)-n)], ...
           ['rows ' num2str(i)]})
    n = n + step + 1;
    i = i + 1;
    pause(0.05)
end

% Raw resolution
width = 6159; % 200464-194305
height = 4032;
fRaw = raw(194305:end);
fRawSquare = reshape(fRaw,[],6160)';


%% Manually reshape array

shapeTest = zeros(4032, 6032);
n = 194305;
step = 200464 - 194305;
i = 1;
while n < length(raw)
    shapeTest(i,:) = raw(n:n+6031);
    n = n + step + 1;
    i = i + 1;
end

%% Test with function
fujiArray = processFujiRaw('DSCF0399.RAF');