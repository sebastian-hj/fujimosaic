function [rawArray] = processFujiRaw(pathToFile)
%PROCESSFUJIRAW Process Fujifilm X-T2 raw to Matlab array
%   There is a long header file, most likely including EXIF information.
%   To do: process header.
%   Pixels are packed in 16-bit little-endian. Each row is 6160 pixels
%   wide, with the last 128 pixels left as an 'overlap'. Each row is in
%   reality 6032 pixels wide and there are 4032 rows in total.
%   Data starts at 194305 (0x2F701)
%
%   INPUT:
%   - pathToFile: path to raw file. Currently support for single file.
%   To do: add support for multiple files
%   OUTPUT:
%   - fujiArray: Matlab array of Fujifilm X-T2 raw.

% Width and height
width = 6032;
height = 4032;
% Real width
realWidth = 6160;

% Start position in original raw file
startPos = 194305;

% Open, read raw file, and close file
fid = fopen(pathToFile,'r');
raw = fread(fid, 'uint16');
fclose(fid);

% Reshape array
raw = raw(startPos:end);
% Data is read as width x height
raw = reshape(raw,realWidth,[]);
% Flip to have height x width
raw = raw.';
% Remove 'overlap' columns
rawArray = raw(:,1:width);

% Get each color channel


end

